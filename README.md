Audit Endpoint Inventory (Linux and Windows)
============================
This composite performs complete inventory on your Linux and Windows endpoint

## Description
This composite scans all linux/windows endpoint and reports on the inventory of objects found


## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_ENDPOINT_INVENTORY_RULES`:
  * description: 
  * default: windows-inventory-endpoint, linux-inventory-endpoint

## Tags


## Categories


## Diagram


## Icon


