
coreo_agent_selector_rule 'check-linux' do
    action :define
    timeout 120
    control 'check-linux' do
        describe os.linux? do
            it { should eq true }
        end
    end
end

coreo_agent_audit_rule 'linux-inventory-endpoint' do
    action :define
    link "http://kb.cloudcoreo.com/mydoc_all-inventory.html"
    display_name 'Linux Endpoint Inventory'
    description 'This rule performs an inventory on the Linux Endpoint using echo hello command'
    category 'Informational'
    suggested_action 'None'
    include_violations_in_count false    
    level 'low'
    selectors ['check-linux']
    timeout 120
    control 'echo-hello' do
      impact 1.0
        describe command('echo hello') do
            its('stdout') { should eq "world\n" }
        end
    end
end

coreo_agent_selector_rule 'check-windows' do
    action :define
    timeout 120
    control 'check-windows' do
        describe os.windows? do
            it { should eq true }
        end
    end
end

coreo_agent_audit_rule 'windows-inventory-endpoint' do
    action :define
    link "http://kb.cloudcoreo.com/mydoc_all-inventory.html"
    display_name 'Windows Endpoint Inventory'
    description 'This rule performs an inventory on the Windows Endpoint using echo hello command'
    category 'Informational'
    suggested_action 'None'
    include_violations_in_count false    
    level 'low'
    selectors ['check-windows']
    timeout 120
    control 'echo-hello' do
        impact 1.0
        describe command('echo hello') do
            its('stdout') { should eq "world\n" }
        end
    end
end

coreo_agent_rule_runner 'audit-endpoint-inventory-rules' do
    action :run
    rules ${AUDIT_ENDPOINT_INVENTORY_RULES}
    filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end
  